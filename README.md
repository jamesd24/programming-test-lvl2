#README#
**Programming Test - Level 2 Software Developer Evaluation**

Author: James Dahms

Email: jamesdahms@gmail.com

Company: Transmax

---

**Program: sort-names**

* Takes a parameter as a string that represents a text file containing a list of names.
* Orders the names by last name followed by first name.
* Creates a new text file called <input-file-name>-sorted.txt with the list of sorted names.

---

**For example, if the input file contains**

BAKER, THEODORE

SMITH, ANDREW

KENT, MADISON

SMITH, FREDRICK

 
**Then the output file would be**

BAKER, THEODORE

KENT, MADISON

SMITH, ANDREW

SMITH, FREDRICK

---

**Example of console execution**


sort-names c:\names.txt

BAKER, THEODORE

KENT, MADISON

SMITH, ANDREW

SMITH, FREDRICK

Finished: created names-sorted.txt