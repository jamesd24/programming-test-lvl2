﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace sort_names.test
{
    [TestClass]
    public class ValidatorTests
    {
        //Validate Arguments Tests
        [TestMethod, TestCategory("Validator")]
        public void ValidatorTest_ValidateNoArguments()
        {
            string[] args = {};
            Assert.IsFalse(Validator.ValidateArgs(args));
        }

        [TestMethod, TestCategory("Validator")]
        public void ValidatorTest_ValidateMultipleArguments()
        {
            string[] args =
            {
                "test.txt",
                "test2.txt",
                "test3.txt"
            };

            Assert.IsFalse(Validator.ValidateArgs(args));
        }

        [TestMethod, TestCategory("Validator")]
        public void ValidatorTest_ValidateOneArgument()
        {
            string[] args = { "names.txt" };
            Assert.IsTrue(Validator.ValidateArgs(args));
        }

        [TestMethod, TestCategory("Validator")]
        public void ValidatorTest_ValidateHelpArgument()
        {
            string[] args = { "?" };
            Assert.IsFalse(Validator.ValidateArgs(args));
        }

        //Validate File Tests
        [TestMethod, TestCategory("Validator")]
        public void ValidatorTest_ValidateTxtFile()
        {
            string[] args = { "names.txt" };
            Assert.IsTrue(Validator.ValidateArgs(args));
            Assert.IsTrue(Validator.ValidateFile(args.First()));
        }

        [TestMethod, TestCategory("Validator")]
        public void ValidatorTest_ValidateFileNotFound()
        {
            string[] args = { "namesnotfound.txt" };
            Assert.IsTrue(Validator.ValidateArgs(args));
            Assert.IsFalse(Validator.ValidateFile(args.First()));
        }

        [TestMethod, TestCategory("Validator")]
        public void ValidatorTest_ValidateFileNotTxt()
        {
            string[] args = { "names.nottxt" };
            Assert.IsTrue(Validator.ValidateArgs(args));
            Assert.IsFalse(Validator.ValidateFile(args.First()));
        }
    }
}

