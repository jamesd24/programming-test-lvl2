﻿using System;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace sort_names.test
{
    [TestClass]
    public class SortTests
    {
        private string[] args = { "names.txt" };
        private string sortedFile = "names-sorted.txt";

        private void EvaluateSortedNames(string[] expectedNames, string[] names)
        {
            File.WriteAllLines(args.First(), names);

            Assert.IsTrue(Validator.ValidateArgs(args));
            Assert.IsTrue(Validator.ValidateFile(args.First()));

            FileProcessor.ProcessFile(args.First());

            Assert.IsTrue(File.Exists(sortedFile));
            Assert.IsTrue(expectedNames.SequenceEqual(File.ReadAllLines(sortedFile)));
        }

        //Sort Tests
        [TestMethod, TestCategory("Sort")]
        public void SortTest_FirstLastCommaNameSortedNames()
        {
            string[] names =
            {
                "BAKER, THEODORE",
                "SMITH, ANDREW",
                "KENT, MADISON",
                "SMITH, FREDRICK"
            };

            string[] expectedNames =
            {
                names[0],
                names[2],
                names[1],
                names[3]
            };

            EvaluateSortedNames(expectedNames, names);
        }

        [TestMethod, TestCategory("Sort")]
        public void SortTest_LastMiddleFirstCommaSortedNames()
        {
            string[] names =
            {
                "KENT, JANE, MADISON",
                "SMITH, ANDY, FREDRICK",
                "BAKER, FRANS, THEODORE",
                "SMITH, ZEN, ANDREW"
            };

            string[] expectedNames =
            {
                names[2],
                names[0],
                names[1],
                names[3]
            };

            EvaluateSortedNames(expectedNames,names);
        }

        [TestMethod, TestCategory("Sort")]
        public void SortTest_LastFirstSpaceSortedNames()
        {
            string[] names =
            {
                "KENT MADISON",
                "SMITH FREDRICK",
                "BAKER THEODORE",
                "SMITH ANDREW"
            };

            string[] expectedNames =
            {
                names[2],
                names[0],
                names[3],
                names[1]
            };

            EvaluateSortedNames(expectedNames, names);
        }

        [TestMethod, TestCategory("Sort")]
        public void SortTest_LastMiddleFirstSpaceSortedNames()
        {
            string[] names =
            {
                "KENT JANE MADISON",
                "SMITH ANDY FREDRICK",
                "BAKER FRANS THEODORE",
                "SMITH ZEN ANDREW"
            };

            string[] expectedNames =
            {
                names[2],
                names[0],
                names[1],
                names[3]
            };

            EvaluateSortedNames(expectedNames, names);
        }

        [TestMethod, TestCategory("Sort")]
        public void SortTest_SimpleSortedNames()
        {
            string[] names =
            {
                "BANNANA",
                "APPLE",
                "TOMATO",
                "CARROT",
                "PINEAPPLE"
            };

            string[] expectedNames =
            {
               names[1],
               names[0],
               names[3],
               names[4],
               names[2]
            };

            EvaluateSortedNames(expectedNames, names);
        }

        [TestMethod, TestCategory("Sort")]
        public void SortTest_NumberSortedNames()
        {
            string[] names =
            {
                "BANNANA",
                "APPLE",
                "1TOMATO",
                "CARROT",
                "2PINEAPPLE"
            };

            string[] expectedNames =
            {
               names[2],
               names[4],
               names[1],
               names[0],
               names[3]
            };

            EvaluateSortedNames(expectedNames, names);
        }

        [TestMethod, TestCategory("Sort")]
        public void SortTest_MixedSortedNames()
        {
            string[] names =
            {
                "BANNANA, 1",
                "BANNANA, 2",
                "KEN THOMPSON",
                "UNIT, TEST",
                "ZEBRA",
                "A T E S T 1,2"
            };

            string[] expectedNames =
            {
               names[5],
               names[0],
               names[1],
               names[2],
               names[3],
               names[4]
            };

            EvaluateSortedNames(expectedNames, names);
        }
    }
}

