﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace sort_names.test
{
    [TestClass]
    public class FileProcessorTests
    {
        [TestMethod, TestCategory("FileProcessor")]
        public void FileTest_WriteFileIOError()
        {
            //Create Names Sorted txt file
            FileProcessor.ProcessFile("names.txt");
            //Lock output file
            var f = File.Open("names-sorted.txt", FileMode.Open, FileAccess.Write, FileShare.None);
            //Try to write over a locked output file
            FileProcessor.ProcessFile("names.txt");
            //Close file
            f.Close();
        }

        [TestMethod, TestCategory("FileProcessor")]
        public void FileTest_ReadFileIOError()
        {
            //Open file with read lock
            var f = File.Open("names.txt", FileMode.Open, FileAccess.Read, FileShare.None);
            //Try to read the locked input file
            FileProcessor.ProcessFile("names.txt");
            //Close file
            f.Close();
        }

        [TestMethod, TestCategory("FileProcessor")]
        public void FileTest_ReadFileDeleted()
        {
            //The input text file could possibly be deleted between validate file and process file.
            string file = "deleted_file.txt";
            Assert.IsFalse(File.Exists(file));
            FileProcessor.ProcessFile("deleted_names.txt");

        }
    }
}
