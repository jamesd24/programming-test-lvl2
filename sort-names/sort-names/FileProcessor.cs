﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace sort_names
{
    public static class FileProcessor
    {
        /// <summary>
        /// Takes an input string representing a path to a file containing a list of names
        /// then writes the list of names in alphabetical order in a separate file (-sorted.txt)
        /// </summary>
        /// <param name="file"></param>
        public static void ProcessFile(string file)
        {
            try
            {
                WriteNames(GetSortedNames(file), file);
            }
            catch (IOException ex)
            {
                Util.PrintError(ex.Message);
            }
        }

        private static string[] GetSortedNames(string file)
        {
            string[] names = new List<string>(File.ReadAllLines(file)).OrderBy((x => x)).ToArray();
            Util.Print(names);
            return names;
        }

        private static void WriteNames(string[] names, string file)
        {
            string sortedFile = file.Replace(".txt", "-sorted.txt");
            File.WriteAllLines(sortedFile, names);
            Util.Print(string.Format("Finished: Created {0}", sortedFile));
        }
    }
}
