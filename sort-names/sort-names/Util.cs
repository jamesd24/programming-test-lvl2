﻿using System;

namespace sort_names
{
    public static class Util
    {
        /// <summary>
        /// Prints string to console stdout.
        /// </summary>
        /// <param name="str"></param>
        public static void Print(string str)
        {
            Console.WriteLine(str);
        }

        /// <summary>
        /// Prints an array of strings to console stdout as separate lines.
        /// </summary>
        /// <param name="str"></param>
        public static void Print(string[] str)
        {
            foreach (string s in str)
            {
                Console.WriteLine(s);
            }
        }

        /// <summary>
        /// Prints string with error outlining.
        /// </summary>
        /// <param name="err"></param>
        public static void PrintError(string err)
        {
            Console.WriteLine("ERROR: {0}",err);
        }

        /// <summary>
        /// Prints the usage of sort-names.exe
        /// </summary>
        public static void PrintUsage()
        {
            Print("Usage: sort-names <FILE.TXT>");
        }
    }
}
