﻿using System.IO;
using System.Linq;
using sort_names.Properties;

namespace sort_names
{
    public static class Validator
    {
        /// <summary>
        /// Validates the input arguments from the command line.
        /// Should only have one argument or a single ? to print help.
        /// </summary>
        /// <param name="args"></param>
        /// <returns>Validation results</returns>
        public static bool ValidateArgs(string[] args)
        {
            if (args.Length > 1 || !args.Any() || args.First() == "?")
            {
                Util.PrintUsage();
                return false;
            }
            return true;
        }

        
        /// <summary>
        /// Validates the input file.
        /// The input file must exist and be a .txt file.
        /// </summary>
        /// <param name="file"></param>
        /// <returns>Validation results</returns>
        public static bool ValidateFile(string file)
        {
            if (!File.Exists(file))
            {
                Util.PrintError(string.Format(AppErrors.FILE_NOT_FOUND_ERROR, file));
                return false;
            }

            if (!IsTxt(file))
            {
                Util.PrintError(string.Format(AppErrors.FILE_TYPE_ERROR, file));
                return false;
            }

            return true;
        }

        private static bool IsTxt(string file)
        {
            return file != null && file.EndsWith(".txt");
        }
    }
}