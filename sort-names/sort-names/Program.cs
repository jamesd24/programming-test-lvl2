﻿using System.Linq;

namespace sort_names
{
    class Program
    {
        static void Main(string[] args)
        {
            if (Validator.ValidateArgs(args) && Validator.ValidateFile(args.First()))
            {
                FileProcessor.ProcessFile(args.First());
            }
        }
    }
}
